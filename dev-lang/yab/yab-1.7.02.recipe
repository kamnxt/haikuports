SUMMARY="Extended version of the yabsic programming language"
DESCRIPTION="
yab is an extended version of yabasic, a BASIC programming language, with \
special commands designed for Haiku.
"
HOMEPAGE="http://sourceforge.net/projects/yab-interpreter"
SRC_URI="https://github.com/HaikuArchives/Yab/archive/v1.7.3.tar.gz"
CHECKSUM_SIZE="1176449"
CHECKSUM_RMD160="f62fa02a585d9f6c5ee3eb9b781a06083352bd6f"
CHECKSUM_SHA512="a74edcf9718ee9a13a16ab0a99fbbed3cceebf1ea4bfac714b098d51531de7fdd0738e126b9a2e5d2f8d84fd0c153d9b7702e806cc91c6a97537dda11411e915"
SOURCE_DIR="Yab-1.7.3"

REVISION="3"
LICENSE="Artistic"
COPYRIGHT="
	1995-2006 Marc-Oliver Ihm (yabasic)
	2006-2009 Jan Bungeroth (yab improvements)
	2013 Jim Saxton (yab improvements)
	"

ARCHITECTURES="x86_gcc2 x86 ?x86_64"

PROVIDES="
	yab = $portVersion compat >=1.7.02 
	cmd:yab= $portVersion compat >=1.7.02 
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libncurses$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libncurses$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:bison
	cmd:flex
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:mkdepend
	cmd:perl
	makefile_engine
	"

BUILD()
{	
	mkdir -p tmp
	cp -r src/* tmp
	cd tmp
	make $jobArgs BUILDHOME=`finddir B_SYSTEM_DEVELOP_DIRECTORY`
	unzip -o App_YAB.zip
	copyattr App_YAB yab
	
}

INSTALL()
{	
	mkdir -p $binDir
	cp tmp/yab  $binDir/
	
	mkdir -p $appsDir/yab-IDE
	cp -r src $appsDir/yab-IDE/src
	cp -r Documentation $appsDir/yab-IDE/Documentation

	mkdir -p $documentationDir
	ln -s $appsDir/yab-IDE/Documentation $documentationDir/yab-1.7.02
}
